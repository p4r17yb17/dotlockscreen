# dotlockscreen

> simple, minimal fork of betterlockscreen

Dotlockscreen allows you to cache images with different filters and lockscreen with blazing speed.

## Example

> lockscreen without any effect

```sh
dotlockscreen --lock
```

![screenshot](https://gitlab.com/p4r17yb17/dotlockscreen/-/raw/master/screenshots/screenshot01.png)

## Table of Contents

- [about](#about)
- [how it works](#how-it-works)
- [requirements](#requirements)
- [installation](#installation)
- [configuration](#configuration)
- [usage](#usage)
- [background](#set-desktop-background-on-startup)
- [keybinding](#keybindings)
- [lockscreen on suspend](#lockscreen-when-suspendedsystemd-service)

### About

Most of i3lock wrapper scripts out there takes an image, adds some effect and locks the screen
adding effects, overall experience doesn't feel natural given delay of 2-3 seconds.
Who would like a delay of 2-3 seconds while locking screen?

So dotlockscreen was my attempt to solve this problem, as we dont need to change lockscreen background frequently
this script caches images with effect so overall experience is simple and as fast as native i3lock.

### How it works

The script takes image adds various effects and caches those images in special directory and then uses those
images as lockscreen background depending on argument provided by user.

### Requirements

> Note: Make sure your system has all dependencies satisfied

- [i3lock-color](https://github.com/Raymo111/i3lock-color) - i3lock fork with additional features( >= 2.11-c )
- [imagemagick](https://www.imagemagick.org/script/index.php) - To apply effects to images
- [xdpyinfo](https://www.x.org/archive/X11R7.7/doc/man/man1/xdpyinfo.1.xhtml), [xrandr](https://www.x.org/wiki/Projects/XRandR/), [bc](https://www.gnu.org/software/bc/) and [feh](https://feh.finalrewind.org/) - To find screen resolution, set custom blur level and wallpaper handling.

### Installation

> manual installation

```sh
git clone https://gitlab.com/p4r17yb17/dotlockscreen
cd dotlockscreen
cp bin/dotlockscreen ~/.local/bin/
```

### Package Manager

#### Arch Linux

###### Installing dependencies(not required if using dotlockscreen PKGBUILD)

`pacman -S imagemagick feh xorg-xrandr xorg-xdpyinfo`

- i3lock-color - AUR `yay -S i3lock-color`

###### Installing with PKGBUILD
```sh
git clone https://gitlab.com/p4r17yb17/dotlockscreen
cd dotlockscreen
makepkg -si
```

### Configuration

You can customise various colors for dotlockscreen, copy config file from examples directory to `~/.config/dotlockscreenrc` and edit it accordingly.

If configuration file is not found then default configurations will be used.

If you have installed dotlockscreen from AUR package, then you can copy default config from docs

```sh
cp /usr/share/doc/dotlockscreen/examples/dotlockscreenrc ~/.config
```

### Usage

Run `dotlockscreen` and point it to either a directory (`dotlockscreen -u "path/to/dir"`) or an image (`dotlockscreen -u "/path/to/img.jpg"`) and that's all. `dotlockscreen` will change update its cache with image you provided.

```sh
usage: dotlockscreen [-u "path/to/img.jpg"] [-l "dim, blur or dimblur"]
           [-w "dim, blur, pixel or dimblur"] [-t "custom text"] [-s "lockscreen and suspend"]
					 [-r "resolution"] [-b "factor"] [--off <timeout>]

dotlockscreen - faster and sweet looking lockscreen for linux systems.

required:
	-u, --update "path/to/img.jpg"	caches all required images

usage:
	-l, --lock effect-name
			locks with provided effect
	-w, --wall effect-name
			set desktop background with provided effect
	-s, --suspend effect-name
			lockscreen and suspend

			Available effects:
				dim, blur, pixel or dimblur

	-t, --text "custom text"
			set custom lockscreen text
	-b, blur 0.0 - 1.0
			set blur range
	-r, --resolution res
			uses a custom resolution
	--off, --off <timeout>
			sets custom monitor timeout (<timeout> in seconds)


Usage examples:
1. Updating image cache(required)
dotlockscreen -u ~/Pictures/Forests.png # caches given image
dotlockscreen -u ~/Pictures             # caches random image from ~/Pictures directory

2. Custom resolution and blur range
dotlockscreen -u path/to/directory -r 1920x1080 -b 0.5

3. Lockscreen
dotlockscreen -l dim                    # lockscreen with dim effect

4. Lockscreen with custom text
dotlockscreen -l pixel -t "custom lockscreen text"

5. Set desktop background
dotlockscreen -w blur                   # set desktop background with blur effect

6. Lockscreeen with custom monitor off timeout
dotlockscreen --off 5 -l blur           # set monitor off lockscreen timeout (5 seconds)
```

### Set desktop background on startup

Add this line to `.xinitrc`.

```sh
# set desktop background with custom effect
dotlockscreen -w dim

# Alternative (set last used background)
source ~/.fehbg
```

#### i3wm

Add this line to `~/.config/i3/config`

```sh
# set desktop background with custom effect
exec --no-startup-id dotlockscreen -w dim

# Alternative (set last used background)
exec --no-startup-id source ~/.fehbg
```

### Keybindings

To lockscreen using keyboard shortcut

#### i3wm

Add this line to your `~/.config/i3/config`

```sh
bindsym $mod+shift+x exec dotlockscreen -l dim
```

#### bspwm

Add this line to your `~/.config/sxhkd/sxhkdrc`

```sh
# lockscreen
alt + shift + x
    dotlockscreen -l dim
```

### Lockscreen when suspended(systemd service)

```sh
# move service file to proper dir (the aur package does this for you)
cp dotlockscreen@.service /etc/systemd/system/

# enable systemd service
systemctl enable dotlockscreen@$USER

# disable systemd service
systemctl disable dotlockscreen@$USER


# Note: Now you can call systemctl suspend to suspend your system
# and dotlockscreen service will be activated
# so when your system wakes your screen will be locked.
```

## License

Dotlockscreen is under [MIT](https://gitlab.com/p4r17yb17/dotlockscreen/-/raw/master/LICENSE) license.
